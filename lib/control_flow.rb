# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  lowercase_chars = ("a".."z").to_a

  str.each_char do |char|
    if lowercase_chars.include?(char)
      str.delete!(char)
    end
  end
  str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  middle = str.length / 2

  str.length % 2 == 0 ? str[middle-1..middle] : str[middle]
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  count = 0
  str.each_char do |char|
    if VOWELS.include?(char)
      count += 1
    end
  end
  count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  1.upto(num).inject(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  string = ""

  arr.each do |elem|
    string << elem + separator
  end
  separator.empty? ? string : string[0...-1]
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  str.chars.map.with_index do |char, i|
    (i + 1) % 2 == 0 ? char.upcase : char.downcase
  end.join
end

# Reverse all words of five or more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  str.split.map do |word|
    if word.length >= 5
      word.reverse
    else
      word
    end
  end.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  fizz_buzz = []
  1.upto(n) do |num|
    if num % 3 == 0 && num % 5 == 0
      fizz_buzz << "fizzbuzz"
    elsif num % 3 == 0
      fizz_buzz << "fizz"
    elsif num % 5 == 0
      fizz_buzz << "buzz"
    else
      fizz_buzz << num
    end
  end
  fizz_buzz
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  reversed = []
  (arr.length-1).downto(0) do |i|
    reversed << arr[i]
  end
  reversed
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1

  2.upto(num-1) do |factor|
    if num % factor == 0
      return false
    end
  end
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  list = []

  1.upto(num) do |factor|
    if num % factor == 0
      list << factor
    end
  end
  list.sort
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors(num).select { |factor| prime?(factor) }
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  even = []
  odd = []

  arr.each do |elem|
    elem.even? ? even << elem : odd << elem
  end
  even.length == 1 ? even[0] : odd[0]
end
